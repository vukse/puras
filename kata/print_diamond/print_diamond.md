# Feature: Print Diamond

As a customer 
I want to give a letter and print a diamond starting with 'A' with the supplied letter at the widest point.


## Scenario_1:

**Given**: Letter 'A' is entered <br>
**When**: the letter is entered <br>
**Then**: 1x1 matrix is printed with diamond shape inside <br>

## Scenario_2:

**Given**: Letter 'C' is entered <br>
**When**: the letter is entered <br>
**Then**: 5x5 matrix is printed with diamond shape inside <br>

## Scenario_3:

**Given**: Letter 'E' is entered <br>
**When**: the letter is entered <br>
**Then**: 9x9 matrix is printed with diamond shape inside <br>

## Example for letter 'E':

....A....<br>
...B.B...<br>
..C...C..<br>
.D.....D.<br>
E.......E<br>
.D.....D.<br>
..C...C..<br>
...B.B...<br>
....A....<br>
